package googleTest

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class StressTest extends Simulation {

	val httpProtocol = http
		.baseURL("https://www.yandex.ru")
//		.inferHtmlResources()
//		.acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
//		.acceptEncodingHeader("gzip, deflate")
//		.acceptLanguageHeader("ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3")
		.userAgentHeader("Chrome/66.0.3359.181")

	object HomeAndVideoPage {
		val open = 
			exec(
				http("yandex.ru")
				.get("/"))
				.exec(
					http("yandex.ru/video/")
					.get("/video/"))
	}

	val scn = scenario("StressTest").exec(HomeAndVideoPage.open)
	setUp(scn.inject(atOnceUsers(100))).protocols(httpProtocol)
}

class LoadTest extends Simulation {

	val httpProtocol = http
		.baseURL("https://www.yandex.ru")
		.userAgentHeader("Chrome/66.0.3359.181")

	object HomeAndVideoPage {
		val open = 
			exec(
				http("yandex.ru")
				.get("/"))
				.exec(
					http("yandex.ru/video/")
					.get("/video/"))
	}

	val scn = scenario("LoadTest").exec(HomeAndVideoPage.open)
	setUp(scn.inject(constantUsersPerSec(10) during (60 seconds))).protocols(httpProtocol)
}